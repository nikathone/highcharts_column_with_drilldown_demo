<?php

namespace Drupal\highcharts_column_with_drilldown_demo\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Render the drilldown demo page.
 */
class DemoController extends ControllerBase {

  public function page() {
    $build = [
      'intro' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('Let build a drilldown highcharts below'),
      ],
    ];
    $build['highcharts-figure'] = [
      '#type' => 'html_tag',
      '#tag' => 'figure',
      '#attached' => [
        'library' => ['highcharts_column_with_drilldown_demo/highcharts'],
      ],
      '#attributes' => [
        'class' => ['highcharts-figure'],
      ],
      'container' => [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'highcharts-figure--container',
        ],
      ],
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Chart showing browser market shares. Clicking on individual columns brings up more detailed data. This chart makes use of the drilldown feature in Highcharts to easily switch between datasets.'),
        '#attributes' => [
          'class' => ['highcharts-figure--description'],
        ],
      ],
    ];
    return $build;
  }

}
